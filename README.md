*Projeto Google Books - Seleção Resultados Digitais*

Projeto de interface para consulta e exibição de livros consultando a API do google Books. O projeto foi desenvolvido usando ReactJS, uma vez que é a tecnologia escolhida pela RD para desenvolver seu principal produto o RDStation (http://shipit.resultadosdigitais.com.br/blog/adotando-react-em-2017/).

*Características do projeto*

- Desenvolvido em React
- App Iniciado utilizando "Create React App" - https://github.com/facebookincubator/create-react-app
- Projeto disponível apenas em modo de "DEV". Testes automatizados e build não realizados.

*Dependências do projeto*

- Bootstrap: Folha de estilos base da página.
- in-array: Usado para verificar um determinado valor existe dentro de um array.
- react-toggle-display: Usado para ocultar e exibir detalhes do livro
- scroll-to-element: usado para navegar para o topo dos resultados da busca após clicar em um dos botões de paginação.

*Executando o projeto*

- Projeto executando apenas em dev
- Baixar o repositório usando o comando "git clone https://diego_deyvison@bitbucket.org/diego_deyvison/g-books.git"
- Executar o comando "npm-install" para instalar as dependências do projeto
- Após finalizar a instalação das dependências, iniciar executando o comando "npm-start"