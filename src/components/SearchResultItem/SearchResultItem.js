import React, { Component } from 'react';
import ToggleDisplay from 'react-toggle-display';

import SearchResultItemDetails from './SearchResultItemDetails';
import FavoriteButton from './FavoriteButton';

export default class SearchResultItem extends Component{

    constructor(props){
        super(props);

        this.state = {
            thumbnail: (props.volumeInfo.imageLinks) ? props.volumeInfo.imageLinks.thumbnail : "https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg",
            authors: (props.volumeInfo.authors) ? props.volumeInfo.authors : [],
            showDetails: false,
            symbol: 'plus'
        };
      }
      
      toggleDetails() {
        this.setState({
            showDetails: !this.state.showDetails
        });

        if(this.state.showDetails){
            this.setState({
                symbol : 'plus'
            })
        }else{
            this.setState({
                symbol : 'minus'
            })   
        }
      } 

    render(){
        return(
            <div className="SearchResultItem col-xs-12">
                <div className="SearchResultItemContent col-md-12">
                    <div className="SearchResultImage col-sm-4 col-md-1">
                        <img onClick={this.toggleDetails.bind(this)} alt={this.props.volumeInfo.title} className="img-responsive" src={this.state.thumbnail} />
                    </div>
                    <div className="SearchResultData col-sm-8 col-md-11">

                        <span className="BookAuthor">{this.state.authors}</span>
                        
                        <h3 className="BookTitle">{this.props.volumeInfo.title}</h3>
                        
                        <span onClick={this.toggleDetails.bind(this)} className="action-bt GoToBookDetails" href="#">
                            <i className={'glyphicon glyphicon-'+this.state.symbol}></i> Detalhes
                        </span>

                        <FavoriteButton bookId={this.props.id}/>

                        <ToggleDisplay show={this.state.showDetails}>
                            <SearchResultItemDetails bookData={this.props.volumeInfo} />
                        </ToggleDisplay>
                    </div>
                </div>
            </div>
        );
    }
}