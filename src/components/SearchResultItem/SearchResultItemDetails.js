import React, { Component } from 'react';

export default class SearchResultItemDetails extends Component{
    render(){
        return(
            <div className="SearchResultItemDetails col-xs-20">
                <p>
                    <strong>Editora:</strong> {this.props.bookData.publisher} <br />
                    <strong>Páginas:</strong> {this.props.bookData.pageCount}
                </p>
                <p><strong>Descrição detalhada</strong></p>
                <p>{this.props.bookData.description}</p>
            </div>
        );
    }
}