import React, { Component } from 'react';

export default class FavoriteButton extends Component{

    constructor(props){
        super(props);

        this.state = {
            isFavorite : this.checkIfIsFavorite(this.props.bookId),
        }

    }

    setFavorite(){
        if(this.state.isFavorite){
            this.setState({isFavorite : false})
            this.removeFavorite(this.props.bookId)
        }else{
            this.setState({isFavorite : true})
            this.addFavorite(this.props.bookId)
        }
    }

    addFavorite(bookId){
        let favorites = this.getFavorites();
        favorites.push(bookId);
        this.saveLocalSotorageFavorites(favorites);
    }

    removeFavorite(bookId){
        let favorites = this.getFavorites();
        var index;
        for (index = 0; index < favorites.length; ++index) {
            if(favorites[index] === bookId){
                favorites.splice(index, 1);
            } 
        }

        this.saveLocalSotorageFavorites(favorites);

    }

    getFavorites(){
        let favorites = [];
        
        if(localStorage.getItem('favoriteBooks')){
            favorites = JSON.parse(localStorage.getItem('favoriteBooks')); 
        }else{
            localStorage.setItem('favoriteBooks', JSON.stringify(favorites));
        }

        return favorites;
    }

    saveLocalSotorageFavorites(favorites){
        localStorage.setItem('favoriteBooks', JSON.stringify(favorites));
    }

    checkIfIsFavorite(){
        let favorites = this.getFavorites();
        var inArray = require('in-array');

        if(inArray(favorites, this.props.bookId)){
            return true;
        }else{
            return false;
        }
        
    }

    render(){
        return(
            <span 
                onClick={this.setFavorite.bind(this)} 
                className={ this.state.isFavorite ? 'FavoriteButton action-bt isFavorite' : 'FavoriteButton action-bt' } 
            >
                <i className="glyphicon glyphicon-star"></i> Favorito
            </span>
        );
    }
}