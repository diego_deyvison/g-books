import React, { Component } from 'react';

import SearchResultItem from './../SearchResultItem/SearchResultItem';
import SearchResultPagination from './SearchResultPagination';

export default class SearchResultList extends Component{
    render(){
        let resultTitle = '';
        let books = [];
        let pagination = [];
        
        if(this.props.books.totalItems > 0){
            books = this.props.books.items;
            resultTitle = 'Livros encontrados para sua busca por "' + this.props.searchText + '"';
            pagination = <SearchResultPagination page={this.props.page} goToPrev={this.props.goToPrev} goToNext={this.props.goToNext} />
        }else if(this.props.books.totalItems === 0 && this.props.searchText){
            resultTitle = 'Não encontramos resultados para a busca "' + this.props.searchText + '"';
        }


        return(
            <section className="SearchResultList" ref={(ref) => this.SearchResultList = ref}>
                <div className="container">
                    <h2 className="SearchResultTitle">{resultTitle}</h2>
                    <div className="row">
                        {books.map(book => <SearchResultItem key={book.id} {...book} />)}
                    </div>
                    {pagination}
                </div>
            </section>
        );
    }
}