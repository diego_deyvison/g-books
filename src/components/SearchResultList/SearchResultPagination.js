import React, { Component } from 'react';

export default class SearchResultList extends Component{
    render(){

        let prevBt = '';
        let nextBt = <span className="nav-bt" onClick={this.props.goToNext}>Próxima</span>;
        let page = this.props.page;

        if(page > 1){
            prevBt = <span className="nav-bt" onClick={this.props.goToPrev}>Anterior</span>;
        }

        return(
            <div className="SearchResultPagination">
                {prevBt}
                <span className="page-ind">{this.props.page}</span>
                {nextBt}
            </div>
        );
    }
}