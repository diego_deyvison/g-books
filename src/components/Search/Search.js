import React, { Component } from 'react';

import './Search.css';

import SearchBox from './SearchBox';
import SearchResultList from './../SearchResultList/SearchResultList';

class Search extends Component {

  constructor(props){
    super(props);
    
    this.state = {
      books: [],
      booksPerPage: 8,
      page: 1,
      searchText: "",
      placeholder: 'Busque pelo seu livro!',
    };

  }

  handleKeyPress(event){
    let self = this;
    let value = event.target.value;
    if(event.key === "Enter" && value){
      self.search(value , 1)
    }
  }

  search(value , pageNumber) {
    let self = this
    if(value && pageNumber){

      let start =  ( pageNumber - 1) * self.state.booksPerPage;

      fetch('https://www.googleapis.com/books/v1/volumes?q=' + value + '&maxResults=' + self.state.booksPerPage + '&startIndex=' + start, {
          method: "GET",
          dataType: 'json'
        })
        .then((r) => r.json())
        .then(books => self.setState({books : books}))
        .then(searchText => self.setState({searchText : value}))
        .then(page => self.setState({page : pageNumber}))
      }
  }

  nextPage(){
    let self = this;
    let nextPage = self.state.page + 1;

    self.search(self.state.searchText , nextPage );
    
    var scrollToElement = require('scroll-to-element'); 
    scrollToElement('.SearchResultList');
  }

  prevPage(){
    let self= this;
    let prevPage = self.state.page - 1;
    
    self.search(self.state.searchText , prevPage );

    var scrollToElement = require('scroll-to-element'); 
    scrollToElement('.SearchResultList');
  }

  render() {
    return (
      <div className="Search">
        <SearchBox 
          getSearchTextOnKeyPress={this.handleKeyPress.bind(this)}
          placeholderText={this.state.placeholder}
        />
        <SearchResultList 
          books={this.state.books}
          page={this.state.page}
          searchText={this.state.searchText}
          goToNext={this.nextPage.bind(this)}
          goToPrev={this.prevPage.bind(this)}
        />
      </div>

    );
  }
}

export default Search;