import React, { Component } from 'react';

export default class SearchBox extends Component {
    render(){
        return(
            <section className="SearchBox">
                <div className="container">
                    <input 
                        onKeyUp={this.props.getSearchTextOnKeyPress}
                        type="text"
                        placeholder={this.props.placeholderText} 
                        className="SearchField form-control"  
                        name="SearchBox" 
                    />
                </div>
            </section>
        );
    }
}