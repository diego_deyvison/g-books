import React, { Component } from 'react';
import './App.css';

import Search from './components/Search/Search';

const appTitle = 'G-Books - Encontre seu livro';

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div className="container">
            <div className="App-logo"><h1 className="App-title">{appTitle}</h1></div>
          </div>
        </header>
        <Search />
      </div>
    );
  }
}